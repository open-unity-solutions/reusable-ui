## [1.3.1] – 2025-01-15

### Added
- Option to add prefix to new message


## [1.3.0] – 2023-07-25

### Added
- `PaginationLabel` and it's listeners, for pagination display
- `TextWrapper` base class, for text display wrappings

### Changed
- `TypewriterTextLabel` and `TwoStepsClickableTextLabel` are now have additive outout


## [1.2.2] – 2023-07-21

### Fixed
- Coroutine replaced with UniTask in `ProgressBar`, so now it runs on disabled object


## [1.2.1] – 2023-07-21

### Added
- `ProgressBar` for simple progress bar


## [1.2.0] – 2023-06-30

### Added
- `TypewriterTextLabel` for letter-by-letter text display
- `TwoStepsClickableTextLabel` for two-step text display

### Changed
- CI/CD updated for modularity


## [1.1.0]

### Changed
- Rebranded to Valhalla


## [1.0.4]

### Fixed
- `CanvasGroupAsync` now correctly handles destroy and cancellation tokens


## [1.0.3]

### Fixed
- `GraphicsTargetRouter` attributes fixed for validation


## [1.0.2]

### Changed
- Now dependent on `com.open-unity-solutions.odin-attributes` 

### Fixed
- `UIFollowUI` now correctly utilize Odin attributes


## [1.0.1]

### Fixed
- Stops UniTask running when object has been destroyed


## [1.0.0]

### Added
- `CanvasGroupAsync` component
- `ClickAwaiter` component
- `UIFollowUI` component
- `RectScaler` component
- `SliceMultiplierSizer` component
