using System;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif


namespace Valhalla.ReusableUI
{
	public class RectScaler : MonoBehaviour
	{
		private RectTransform _selfRect;
		private RectTransform _targetRect;
        
        
		private void Awake()
		{
			_selfRect = GetComponent<RectTransform>();
		}


		public void ScaleTo(RectTransform target, float add)
		{
			_targetRect = target;

			Rescale(add);
		}

		#if ODIN_INSPECTOR
		[Button]
		#endif
		private void Rescale(float add)
		{
			if (add < 0)
				throw new ArgumentException($"Arg must be >= 0", nameof(add));
			
			var targetSize = _targetRect.rect.size;
			var selfSize = _selfRect.rect.size;
			
			var scaleDeltaY = Mathf.Abs((targetSize.y * (add+1)) / (selfSize.y));
			var scaleDeltaX = Mathf.Abs((targetSize.x * (add+1)) / (selfSize.x));
			
			var result = new Vector2(scaleDeltaX * _selfRect.rect.width, scaleDeltaY * _selfRect.rect.height);

			if (result.x < 1)
				result.Set(1, result.y);
			
			if (result.y < 1)
				result.Set(result.x, 1);
			
			_selfRect.sizeDelta = result;
			
		}
        
	}
}
