﻿using System.Threading;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif


namespace Valhalla.ReusableUI
{
	[RequireComponent(typeof(CanvasGroup))]
	public class CanvasGroupAsync : MonoBehaviour
	{
		[SerializeField]
		private bool _blocksRaycastWhenVisible = true;


		[SerializeField]
		private bool _blocksRaycastWhenHidden;


		private CanvasGroup _canvasGroup;

		private CancellationToken _cancellationTokenOnDestroy;
		
		public float Alpha
		{
			get => _canvasGroup.alpha;

			set
			{
				if (_canvasGroup == null)
					_canvasGroup = GetComponent<CanvasGroup>();

				_canvasGroup.alpha = value;

				UpdateBlockRaycast();
			}
		}


		public bool BlocksRaycasts
		{
			get
			{
				if (IsHidden)
					return _blocksRaycastWhenHidden;

				return _blocksRaycastWhenVisible;
			}
		}


		public bool IsHidden => Alpha <= 0.01;


		public bool IsShown => Alpha >= 0.99;
		


		private void Awake()
		{
			_canvasGroup = GetComponent<CanvasGroup>();
			_cancellationTokenOnDestroy = this.GetCancellationTokenOnDestroy();
			
			UpdateBlockRaycast();
		}



		private void UpdateBlockRaycast()
		{
			_canvasGroup.blocksRaycasts = BlocksRaycasts;
		}


		#if ODIN_INSPECTOR
		[Button]
		#endif
		public void Show()
			=> Alpha = 1;


		public async UniTask ShowAsync(int milliseconds)
			=> await FadeAsync(milliseconds, true, _cancellationTokenOnDestroy);


		public async UniTask ShowAsync(int milliseconds, CancellationToken token)
			=> await FadeAsync(milliseconds, true, token);


		#if ODIN_INSPECTOR
		[Button]
		#endif
		public void Hide()
			=> Alpha = 0;


		public async UniTask HideAsync(int milliseconds)
			=> await FadeAsync(milliseconds, false, _cancellationTokenOnDestroy);


		public async UniTask HideAsync(int milliseconds, CancellationToken token)
			=> await FadeAsync(milliseconds, false, token);


		private async UniTask FadeAsync(int milliseconds, bool isIn, CancellationToken token)
		{
			token = CombineTokensWithOnDestroy(token);

			if (!isIn && IsHidden || isIn && IsShown)
				return;

			if (token.IsCancellationRequested)
				return;


			var (initial, final) = isIn
				? (0f, 1f)
				: (1f, 0f);

			_canvasGroup.alpha = initial;

			await DOTween
				.To(
					() => token.IsCancellationRequested
						? final
						: _canvasGroup.alpha,
					f =>
					{
						if (token.IsCancellationRequested)
							return;

						_canvasGroup.alpha = f;
					},
					final,
					milliseconds / 1000f
				)
				.WithCancellation(token);

			if (token.IsCancellationRequested)
				return;

			UpdateBlockRaycast();
		}


		private CancellationToken CombineTokensWithOnDestroy(CancellationToken token)
		{
			if (token == _cancellationTokenOnDestroy)
				return token;

			var cts = CancellationTokenSource.CreateLinkedTokenSource(token, _cancellationTokenOnDestroy);
			return cts.Token;
		}
	}
}
