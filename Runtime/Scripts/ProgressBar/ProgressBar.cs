using System;
using System.Collections;
using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Valhalla.UniTaskExtension;


namespace Valhalla.ReusableUI
{
	public class ProgressBar : MonoBehaviour
	{
		[HideInInspector]
		private readonly UnityEvent<int, int> OnValueChaged = new();
		
		
		[SerializeField, ChildGameObjectsOnly, Required]
		[BoxGroup("Setup")]
		private Scrollbar _slider;


		[SerializeField]
		[BoxGroup("Parameters")]
		private bool _animatedChangeEnabled = true;
		
		
		[SerializeField]
		[BoxGroup("Parameters"), ShowIf(nameof(_animatedChangeEnabled))]
		[Range(0.1f, 5f)]
		private float _speed = 0.2f;
		
		[SerializeField]
		[BoxGroup("Parameters"), ShowIf(nameof(_animatedChangeEnabled))]
		[Range(0.1f, 5f)]
		private float _negativeSpeed = 1f;

		[ShowInInspector]
		[BoxGroup("Runtime")]
		private float _targetSize = 0;


		protected void Start()
		{
			if (_animatedChangeEnabled)
				ValueUpdateLoopAsync(this.GetCancellationTokenOnDestroy()).ForgetWithHandler();
		}


		protected void OnValidate()
		{
			if (_slider == null)
				return;


			_slider.interactable = false;
			_slider.transition = Selectable.Transition.None;
		}


		[Button]
		public void Setup(int value, int maxValue)
		{
			SetMaxValue(maxValue);
			SetTargetSize(value);

			RedrawSlider(_targetSize);
		}



		public virtual void UpdateValue(int value)
		{
			SetTargetSize(value);

			if (!_animatedChangeEnabled || !Application.isPlaying)
				RedrawSlider(_targetSize);
			
			OnValueChaged.Invoke(value, _slider.numberOfSteps);
		}

		
		protected virtual void SetMaxValue(int maxValue)
		{
			_slider.numberOfSteps = maxValue;
		}
		
		
		protected virtual void SetTargetSize(int value)
		{
			_targetSize = value / (float) _slider.numberOfSteps;
		}


		private async UniTask ValueUpdateLoopAsync(CancellationToken cancellationToken)
		{
			while (!cancellationToken.IsCancellationRequested)
			{
				if (this.isActiveAndEnabled && Mathf.Abs(_slider.size - _targetSize) > 0.0001)
				{
					bool isLessThanTarget = _slider.size < _targetSize;
					
					var speed = isLessThanTarget
						? _speed
						: _negativeSpeed;
					
					var sign = Mathf.Sign(_targetSize - _slider.size);
					var nextSliderSize = _slider.size + sign * Time.deltaTime * speed;

					if (isLessThanTarget && nextSliderSize > _targetSize)
						nextSliderSize = _targetSize;
					else if (!isLessThanTarget && nextSliderSize < _targetSize)
						nextSliderSize = _targetSize;
					
					RedrawSlider(nextSliderSize);
				}

				await UniTask.DelayFrame(1, cancellationToken: cancellationToken).SuppressCancellationThrow();
			}
		}
		
		
		protected void RedrawSlider(float size)
		{
			_slider.size = size;
			_slider.value = 0;
		}
	}
}
