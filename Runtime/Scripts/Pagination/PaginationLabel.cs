using System;
using System.Collections;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Valhalla.EveryDay.Extensions;
using Valhalla.ReusableUI.Wrappers;


namespace Valhalla.ReusableUI.Pagination
{
	public class PaginationLabel : TextWrapper
	{
		[HideInInspector]
		public readonly UnityEvent<int, int> OnPaginationStateChanged = new();



		private int _pageCount = 1;
		
		private int _currentPage = 1;
		

		[ShowInInspector]
		[BoxGroup("State")]
		public int PageCount
			=> _pageCount;
		
		[ShowInInspector]
		[BoxGroup("State")]
		[PropertyRange(0, nameof(PageCount))]
		public int CurrentPage
		{
			get => _currentPage;

			set
			{
				#if UNITY_EDITOR
				if (_label == null)
					return;
				#endif

				StartCoroutine(UpdateAndSetCurrentPage(value));
			}
		}

		
		public override string Text
		{
			set
			{
				_label.text = value;

				StartCoroutine(UpdateAndSetCurrentPage(CurrentPage));
			}
		}
		

		protected IEnumerator Start()
		{
			OnPaginationStateChanged.Invoke(CurrentPage, PageCount);
			
			yield return UpdateAndSetCurrentPage(CurrentPage);
		}


		private IEnumerator UpdateAndSetCurrentPage(int pageToSet)
		{
			var prevPageCount = _pageCount;
			var prevPage = _currentPage;

			yield return null;
			
			_pageCount = _label.textInfo.pageCount;
			_currentPage = pageToSet.ClampValue(1, _pageCount);
			
			_label.pageToDisplay = _currentPage;

			if (prevPageCount != _pageCount || prevPage != _currentPage)
				OnPaginationStateChanged.Invoke(CurrentPage, PageCount);
		}
		

		protected void OnValidate()
		{
			if(_label == null)
				return;

			_label.overflowMode = TextOverflowModes.Page;
		}
		
		
		public void _OnNextClick()
		{
			CurrentPage++;
		}
		
		public void _OnPreviousClick()
		{
			CurrentPage--;
		}
	}
}
