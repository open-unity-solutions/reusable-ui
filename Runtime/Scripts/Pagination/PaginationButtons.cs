using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;


namespace Valhalla.ReusableUI.Pagination
{
	public class PaginationButtons : MonoBehaviour
	{
		[SerializeField, Required]
		[BoxGroup("Setup")]
		private PaginationLabel _sourcePagination;
		
		[SerializeField, Required]
		[BoxGroup("Setup")]
		private Button _previousPageButton;
		
		[SerializeField, Required]
		[BoxGroup("Setup")]
		private Button _nextPageButton;
		
		
		protected void Awake()
		{
			_sourcePagination.OnPaginationStateChanged.AddListener(OnPaginationStateChange);
			
			_previousPageButton.onClick.AddListener(_sourcePagination._OnPreviousClick);
			_nextPageButton.onClick.AddListener(_sourcePagination._OnNextClick);
		}


		protected void OnDestroy()
		{
			_sourcePagination.OnPaginationStateChanged.RemoveListener(OnPaginationStateChange);
			
			_previousPageButton.onClick.RemoveListener(_sourcePagination._OnPreviousClick);
			_nextPageButton.onClick.RemoveListener(_sourcePagination._OnNextClick);
		}


		private void OnPaginationStateChange(int page, int pageCount)
		{
			_previousPageButton.interactable = page > 1;
			_nextPageButton.interactable = page < pageCount;
		}
	}
}
