using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;


namespace Valhalla.ReusableUI.Pagination
{
	public class PageCountLabel : MonoBehaviour
	{
		[SerializeField, Required]
		[BoxGroup("Setup")]
		private PaginationLabel _sourcePagination;
		
		
		[SerializeField, ChildGameObjectsOnly, Required]
		[BoxGroup("Setup")]
		private TMP_Text _label;
		
		
		protected void Awake()
		{
			_sourcePagination.OnPaginationStateChanged.AddListener(OnPaginationStateChange);
		}


		protected void OnDestroy()
		{
			_sourcePagination.OnPaginationStateChanged.RemoveListener(OnPaginationStateChange);
		}


		private void OnPaginationStateChange(int page, int pageCount)
		{
			_label.text = $"{page}/{pageCount}";
		}
	}
}
