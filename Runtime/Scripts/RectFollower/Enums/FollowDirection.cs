namespace Valhalla.ReusableUI
{
	public enum FollowDirection
	{
		Center,
		Left,
		Right,
		Top,
		Bottom,
	}
}
