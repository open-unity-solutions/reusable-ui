using JetBrains.Annotations;
using Valhalla.OdinAttributes;
using UnityEngine;


#if ODIN_INSPECTOR
using Sirenix.OdinInspector;
#endif

namespace Valhalla.ReusableUI
{
	[RequireComponent(typeof(RectTransform))]
	public class UIFollowsUI : MonoBehaviour
	{
		[SerializeField]
		#if ODIN_INSPECTOR
		[SceneObjectsOnly, CanBeNull]
		#endif
		private RectTransform _target;


		[SerializeField]
		private int _offset;


		[SerializeField]
		private FollowDirection _direction;


		[SerializeField]
		#if ODIN_INSPECTOR
		[SelfOnly, Required]
		#endif
		private RectTransform _selfRect;


		private void LateUpdate()
		{
			if (_target == null)
				return;

			var targetPos = _target.position;
			var targetRect = _target.rect;
			var selfRect = _selfRect.rect;

			var x = targetPos.x;
			var y = targetPos.y;

			switch (_direction)
			{
				case FollowDirection.Left:
					x -= (targetRect.width + selfRect.width) / 2f + _offset;

					break;

				case FollowDirection.Right:
					x += (targetRect.width + selfRect.width) / 2f + _offset;

					break;

				case FollowDirection.Top:
					y += (targetRect.height + selfRect.height) / 2f + _offset;

					break;

				case FollowDirection.Bottom:
					y -= (targetRect.height + selfRect.height) / 2f + _offset;

					break;
			}


			_selfRect.position = new Vector3(x, y);
		}


		public void SetTarget(RectTransform tf)
		{
			_target = tf;
		}


		public void SetDirection(FollowDirection dir)
		{
			_direction = dir;
		}
	}
}
