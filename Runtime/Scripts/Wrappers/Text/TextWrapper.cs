using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Valhalla.OdinAttributes;


namespace Valhalla.ReusableUI.Wrappers
{
	public class TextWrapper : MonoBehaviour
	{
		[SerializeField, SelfOnly, Required]
		[BoxGroup("Steup")]
		protected TMP_Text _label;


		public TMP_Text Label
			=> _label;
		
		
		[ShowInInspector, MultiLineProperty(20)]
		[BoxGroup("State")]
		public virtual string Text
		{
			get
			{
				#if UNITY_EDITOR
				if (_label == null)
					return null;
				#endif
				
				return _label.text;
			}

			set => _label.text = value;
		}
	}
}
