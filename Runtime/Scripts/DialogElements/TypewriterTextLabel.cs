using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using Valhalla.ReusableUI.Wrappers;


namespace Valhalla.ReusableUI
{
	public class TypewriterTextLabel : MonoBehaviour
	{
		[SerializeField, ChildGameObjectsOnly, Required]
		[BoxGroup("Setup")]
		private TextWrapper _messageTextLabel;


		[SerializeField]
		[BoxGroup("Parameters")]
		private int _millisecondsDelay = 10;

		[SerializeField]
		[BoxGroup("Parameters")]
		private int _charactersPerOutput = 1;

		[SerializeField]
		[BoxGroup("Parameters")]
		private string _newMessagePrefix = "<b>";
		
		[ShowInInspector, ReadOnly]
		[BoxGroup("Runtime")]
		private string _message;


		private void Awake()
		{
			ClearText();
		}


		public async UniTask SetupAndDisplayMessageAdditiveAsync([CanBeNull] string message, string separator, bool isPrefixNeeded, CancellationToken cancellationToken = default)
		{
			PreRunSetupAdditive(message, separator, isPrefixNeeded);
			await DisplayMessageAsync(cancellationToken);
		}
		
		
		public void PreRunSetupAdditive([CanBeNull] string message, string separator, bool isPrefixNeeded)
		{
			var haveText = !string.IsNullOrEmpty(message) || !string.IsNullOrEmpty(_message);

			gameObject.SetActive(haveText);

			SetMessageInvisibleAdditive(message, separator, isPrefixNeeded);
		}


		private void SetMessageInvisibleAdditive(string message, string separator, bool isPrefixNeeded)
		{
			if (_message == null)
			{
				_message = "";
				separator = "";
			}
			else if (separator == null)
				separator = "";
			
			_messageTextLabel.Label.maxVisibleCharacters =  _message.Length;
			_message                                     += separator;
			_messageTextLabel.Text                       =  _message + (isPrefixNeeded ? _newMessagePrefix : "") + message;
			_message                                     += message;
		}

		
		public async UniTask DisplayMessageAsync(CancellationToken token = default)
		{
			if (string.IsNullOrEmpty(_message))
			{
				ClearText();

				return;
			}

			var isCanceled =
				await UniTask.Delay(_millisecondsDelay, cancellationToken: token, delayType: DelayType.Realtime).SuppressCancellationThrow();

			if (isCanceled)
			{
				_messageTextLabel.Label.maxVisibleCharacters = int.MaxValue;

				return;
			}

			int offset = _messageTextLabel.Label.maxVisibleCharacters;
			
			for (var chIndex = offset; chIndex < _message.Length; chIndex+=_charactersPerOutput)
			{
				if (token.IsCancellationRequested)
					break;

				_messageTextLabel.Label.maxVisibleCharacters = chIndex;

				isCanceled = await UniTask.Delay(_millisecondsDelay, cancellationToken: token, delayType: DelayType.Realtime).SuppressCancellationThrow();

				if (isCanceled)
					_messageTextLabel.Label.maxVisibleCharacters = int.MaxValue;
			}

			_messageTextLabel.Label.maxVisibleCharacters = int.MaxValue;
		}


		public void ClearText()
		{
			_messageTextLabel.Text = "";
			_message = "";
		}
	}
}
