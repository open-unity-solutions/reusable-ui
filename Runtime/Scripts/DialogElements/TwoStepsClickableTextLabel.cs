using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Valhalla.ReusableUI
{
	public class TwoStepsClickableTextLabel : MonoBehaviour
	{
		[SerializeField, ChildGameObjectsOnly, Required]
		[BoxGroup("Setup")]
		private TypewriterTextLabel _typewriterTextLabel;
		
		private CancellationTokenSource _displayMessageCancellationTokenSource;
		private CancellationTokenSource _clickAwaitCancellationTokenSource;

		private bool _isClicked;
		

		protected void Start()
		{
			_displayMessageCancellationTokenSource = new CancellationTokenSource();
		}


		public async UniTask DisplayTextAdditiveAsync(string message, string separator, bool isPrefixNeeded)
		{
			_displayMessageCancellationTokenSource = new CancellationTokenSource();
			await _typewriterTextLabel.SetupAndDisplayMessageAdditiveAsync(message, separator, isPrefixNeeded, _displayMessageCancellationTokenSource.Token);

			await WaitForClickAsync();
		}


		public void ClearText()
		{
			_typewriterTextLabel.ClearText();
		}
		
		
		private async UniTask WaitForClickAsync()
		{
			_clickAwaitCancellationTokenSource = new CancellationTokenSource();

			_isClicked = false;
			// TODO rewind
			await UniTask.WaitUntil(() => _isClicked, cancellationToken: _clickAwaitCancellationTokenSource.Token);
			
			_clickAwaitCancellationTokenSource.Cancel();
		}


		[Button, DisableInEditorMode]
		public void Click()
		{
			_isClicked = true;
			
			SkipLetterOutput();
		}
		
		private void SkipLetterOutput()
		{
			if (_displayMessageCancellationTokenSource != null && !_displayMessageCancellationTokenSource.IsCancellationRequested)
				_displayMessageCancellationTokenSource.Cancel();
		}
	}
}
